import frappe


@frappe.whitelist(allow_guest=True)
def apitest(param1,param2):
    return int(param1)-int(param2)