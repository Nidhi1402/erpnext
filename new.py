# -*- coding: utf-8 -*-
# Copyright (c) 2021, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class new(Document):
	pass

@frappe.whitelist(allow_guest=True)		
def newone():
	doc = frappe.new_doc('new')
	doc.name_name = 'prisha2'
	doc.class_class = '3'
	doc.section_section = 'B'
	doc.save(ignore_permissions=True)
	frappe.db.commit()
	return "new has been created successfully"
