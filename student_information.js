// Copyright (c) 2021, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on("student information", "validate", function(frm) {
	if (frm.doc.roll_number < 10){
		frappe.msgprint(__("Enter a valid roll number"));
		frappe.validated = false;

	 }
});

// frappe.ui.form.on("student information", "validate", function(frm) {
// 	if (frm.doc.contact.length < 10){
// 		frappe.msgprint(__("Enter a valid contact number"));
// 		frappe.validated = true;

// 	 }
// });



