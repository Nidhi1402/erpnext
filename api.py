import frappe

@frappe.whitelist(allow_guest=True)
def value():
    return frappe.get_value("student_information", "mishka", "roll_number")

